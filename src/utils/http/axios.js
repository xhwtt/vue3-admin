import axios from 'axios'
import { errorHandler, errorMsgHandler } from './errorHandler'

const service = axios.create({
	timeout: 1000 * 30,
	baseURL: import.meta.env.VITE_BASE_URL
})

service.interceptors.request.use(
	(config) => {
		// get请求映射params参数
		if (config.method === 'get' && config.params) {
			let url = config.url + '?'
			for (const propName of Object.keys(config.params)) {
				const value = config.params[propName]
				var part = encodeURIComponent(propName) + '='
				if (value !== null && typeof value !== 'undefined') {
					if (typeof value === 'object') {
						for (const key of Object.keys(value)) {
							let params = propName + '[' + key + ']'
							var subPart = encodeURIComponent(params) + '='
							url += subPart + encodeURIComponent(value[key]) + '&'
						}
					} else {
						url += part + encodeURIComponent(value) + '&'
					}
				}
			}
			url = url.slice(0, -1)
			config.params = {}
			config.url = url
		}
		// ... 后续增加请求头
		return config
	},
	(error) => {
		return Promise.reject()
	}
)

service.interceptors.response.use((response) => {
	const config = response.config
	if (response.status === 200) {
		if (config?.isReturnNativeData) {
			return response.data
		} else {
			const { code, message } = response.data
			if (code === 0) {
				return Promise.resolve(response.data)
			} else {
				errorHandler(message || errorMsgHandler(code), config.errorMode)
				return Promise.reject()
			}
		}
	} else {
		const errMsg = errorMsgHandler(response.status)
		return Promise.reject()
	}
})

// 错误处理
service.interceptors.response.use(undefined, (e) => {
	errorHandler(e || '')
	return Promise.reject()
})

export default service
