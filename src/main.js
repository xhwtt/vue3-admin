import { createApp } from 'vue/dist/vue.esm-bundler'
import { createPinia } from 'pinia'
import './permission.js'
import ElementPlus from 'element-plus'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
import 'element-plus/dist/index.css'
import '@/assets/css/common.css'
import '@/assets/css/element.css'
import STable from '@/components/STable/index.vue'

import App from './App.vue'
import router from './router'

const app = createApp(App)

app.component('STable', STable)

app.use(createPinia())
app.use(router)
app.use(ElementPlus, {
	locale: zhCn
})
app.mount('#app')
