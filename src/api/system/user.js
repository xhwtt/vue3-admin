import request from '@/utils/http/axios.js'

// 查看
export function getList(params) {
	return request({
		url: '/api/user/list',
		method: 'get',
		params
	})
}

// 添加
export function add(data) {
	return request({
		url: '/api/user/add',
		method: 'post',
		data
	})
}

// 删除
export function onDelete(id) {
	return request({
		url: '/api/user/delete',
		method: 'get',
		params: {
			id
		}
	})
}
// 获取详情
export function getDetail(id) {
	return request({
		url: '/api/user/details',
		method: 'get',
		params: {
			id
		}
	})
}

// 更新数据
export function update(data) {
	return request({
		url: '/api/user/update',
		method: 'post',
		data
	})
}
