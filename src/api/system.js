import request from '@/utils/http/axios.js'

// 查看
export function getList(id) {
	return request({
		url: '/api/depts/list',
		method: 'get'
	})
}

// 添加
export function add(data) {
	return request({
		url: '/api/depts/add',
		method: 'post',
		data
	})
}

// 删除
export function onDelete(id) {
	return request({
		url: '/api/depts/delete',
		method: 'get',
		params: {
			id
		}
	})
}
// 获取详情
export function getDetail(id) {
	return request({
		url: '/api/depts/details',
		method: 'get',
		params: {
			id
		}
	})
}

// 更新数据
export function update(data) {
	return request({
		url: '/api/depts/update',
		method: 'post',
		data
	})
}
