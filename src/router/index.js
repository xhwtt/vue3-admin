import { createRouter, createWebHistory } from 'vue-router'
import Layout from '@/layout/index.vue'

const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: [
		{
			path: '/redirect',
			component: Layout,
			hidden: true,
			children: [
				{
					path: '/redirect/:path(.*)',
					component: () => import('@/views/redirect/index.vue')
				}
			]
		},
		{
			path: '/',
			component: Layout,
			redirect: '/index',
			children: [
				{
					path: '/index',
					component: () => import('@/views/home/index.vue'),
					name: 'index',
					meta: { title: '首页', icon: 'dashboard' }
				},
				{
					path: '/dept',
					component: () => import('@/views/system/dept.vue'),
					name: 'dept',
					meta: { title: '部门', icon: 'dashboard' }
				},
				{
					path: '/user',
					component: () => import('@/views/system/user.vue'),
					name: 'user',
					meta: { title: '用户', icon: 'dashboard' }
				}
			]
		},

		{
			path: '/:catchAll(.*)',
			redirect: '/404'
		},
		{
			path: '/404',
			component: () => import('@/components/ErrorMessage/404.vue')
		}
	]
})

export default router
