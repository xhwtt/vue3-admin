### **Vue3使用内联模版字符串**

```vue
<template>
  <div>
    <component :is="MyComponent"></component>
  </div>
</template>
<script setup lang="jsx">
const MyComponent = defineComponent({
  template: `
    <div>
      <h2>{{ title }}</h2>
      <p>{{ content }}</p>
    </div>
  `,
  data() {
    return {
      title: 'Hello',
      content: 'This is my component'
    }
  }
})
</script>
```

