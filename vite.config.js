import { fileURLToPath, URL } from 'node:url'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import AutoImport from 'unplugin-auto-import/vite' // 自动引入vue api

export default defineConfig({
	plugins: [
		vue(),
		vueJsx(),
		AutoImport({
			include: [
				/\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
				/\.vue$/,
				/\.vue\?vue/, // .vue
				/\.md$/ // .md
			],
			eslintrc: {
				enabled: false // 解决eslint中ref等引入报错问题
			},
			resolvers: [],
			imports: ['vue', 'vue-router'],
			dts: 'src/auto-import.d.ts'
		})
	],
	esbuild: {
		jsxFactory: 'h',
		jsxFragment: 'Fragment'
	},
	resolve: {
		alias: {
			'@': fileURLToPath(new URL('./src', import.meta.url))
		}
	},
	server: {
		port: 8081, //启动端口
		host: true,
		proxy: {
			'/api': {
				target: 'http://localhost:8080',
				changeOrigin: true,
				rewrite: (path) => path.replace(/^\/api/, '')
			}
		}
	}
})
